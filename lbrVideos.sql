-- phpMyAdmin SQL Dump
-- version 3.5.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2012 at 08:20 AM
-- Server version: 5.5.27-log
-- PHP Version: 5.4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lbrVideos`
--

-- --------------------------------------------------------

--
-- Table structure for table `uploadadmin`
--

CREATE TABLE IF NOT EXISTS `uploadadmin` (
  `UserName` varchar(20) NOT NULL,
  `Password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uploadadmin`
--

INSERT INTO `uploadadmin` (`UserName`, `Password`) VALUES
('test', 'test123');

-- --------------------------------------------------------

--
-- Table structure for table `videodata`
--

CREATE TABLE IF NOT EXISTS `videodata` (
  `VideoType` varchar(7) NOT NULL,
  `VideoTitle` varchar(30) NOT NULL,
  `VideoDescription` varchar(250) NOT NULL,
  `VideoDate` date NOT NULL,
  `VideoThumbImage` varchar(75) NOT NULL,
  `VideoPath` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videodata`
--

INSERT INTO `videodata` (`VideoType`, `VideoTitle`, `VideoDescription`, `VideoDate`, `VideoThumbImage`, `VideoPath`) VALUES
('LBR', 'LBR Discussion', 'Test Duminda Rajitha Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Test Duminda Rajitha', '2012-09-08', 'ewrt.jpg', 'ewrt.flv'),
('LBR', 'LBR Discussion fake2', 'Test Duminda Rajitha Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Test Duminda Rajitha', '2012-09-11', 'ewrt.jpg', 'ewrt.flv'),
('LBR', 'LBR Discussion fake2', 'Test Duminda Rajitha Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Discusiion test goes here Discusiion test goes here Discusiion test goes here test !test!test !Test Duminda Rajitha', '2012-09-11', 'ewrt.jpg', 'ewrt.flv');

-- --------------------------------------------------------

--
-- Table structure for table `vmscatagory`
--

CREATE TABLE IF NOT EXISTS `vmscatagory` (
  `Catagory` varchar(10) NOT NULL,
  `VideoLocation` varchar(50) NOT NULL,
  PRIMARY KEY (`Catagory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vmscatagory`
--

INSERT INTO `vmscatagory` (`Catagory`, `VideoLocation`) VALUES
('LBO', 'uploads/lbo/'),
('LBR', 'uploads/lbr/');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
