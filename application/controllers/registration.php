<?php

class Registration extends CI_Controller {

	function __construct()
	{
 		parent::__construct();
		$this->load->library('form_validation');
                $this->load->library('session');
		$this->load->database();
		$this->load->helper('form');
                $this->load->helper('array');
		$this->load->helper('url');
		$this->load->model('registrationmodel');
	}
	function index()
	{
                //session_destroy();
            
                $uname = $this->input->post('username');
                $pw = $this->input->post('password');
            
		$this->form_validation->set_rules('username', 'UserName', 'required|max_length[25]');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[25]|matches[repassword]');
                $this->form_validation->set_rules('repassword', 'Password Confirmation', 'required');

		$this->form_validation->set_error_delimiters('<div class="error" style="position: relative;color:red">', '</div>');

		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			$this->load->view('userRegister');
		}
                else{
                    $data=$this->registrationmodel->Register($uname,$pw);
                    
                    if(!$data){
                        $this->load->view('userRegister','Failed to Register User '.$uname);
                    }
                    
                    $this->load->view('authentication');
                    
                }
		
	}

}
?>