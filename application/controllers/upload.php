<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Upload extends CI_Controller {

public function __construct()
{
    parent::__construct();
    $this->load->model('uploadmodel');
}

function index()
{
    if($this->session->userdata('logged_in'))
    {
        ini_set('max_execution_time', 3600);
        $this->form_validation->set_rules('videotitle', 'Name', 'required|max_length[25]');
        $this->form_validation->set_rules('videodescription', 'Description', 'required|max_length[100]');

        $this->form_validation->set_error_delimiters('<div class="error" style="position: relative;color:red">', '</div>');

        if ($this->form_validation->run() == FALSE) // validation hasn't been passed
        {
                $this->load->view('uploadVideo');
        }
        else // passed validation proceed to post success logic
        {

        //file upload
        $catagory = $this->input->post('videocategory');
        $uplocation=$this->uploadmodel->RetriveVideoLocation($catagory);

        $uppath=$uplocation[0]["VideoLocation"];

        $config['upload_path'] = './'.$uppath;
        $config['allowed_types'] = 'mpeg';
        $config['max_size'] = '9992043900';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        //!$this->upload->do_upload('videothumb') ||

        if(!$this->upload->do_upload('video')){
            
         $error=array('error'=>$this->upload->display_errors());
         $this->load->view('upload-err',$error);
            //var_dump($error);
        }else{
        //$this->redirect('upload-err');
        $data = array('upload_data' => $this->upload->data());


        $file=$data["upload_data"]["file_name"];

        $newfilename = $this->input->post('videoname');

        if($newfilename){

           preg_replace("/[^A-Za-z0-9]/","",$newfilename);
           str_replace (' ', '_',$newfilename);

            $newvideofile=$uppath.$newfilename.'.flv';
            $newthumbfile=$uppath.$newfilename.'.jpg';

            $vf=$newfilename.'.flv';
            $if=$newfilename.'.jpg';

        }

        if(!$newfilename){

            $withoutExt = preg_replace("/\\.[^.\\s]{3,4}$/", "", $file);

            $newvideofile=$uppath.$withoutExt.'.flv';
            $newthumbfile=$uppath.$withoutExt.'.jpg';

            $if=$withoutExt.'.jpg';
            $vf=$withoutExt.'.flv';
        }

        set_time_limit(0);

        $video_path=$_FILES["video"]["name"];
        $video_full_path=$uppath.$video_path;

        $vsizew = $this->input->post('width');
        $vsizeh = $this->input->post('height');

        exec('ffmpeg -i '.$video_full_path.' -deinterlace -an -ss 100 -t 00:00:10 -r 1 -y -vcodec mjpeg -f mjpeg -s '.$vsizew.'x'.$vsizeh.' '.$newthumbfile.' 2>&1');
        exec("ffmpeg -i ".$video_full_path." -deinterlace -ar 44100 -r 30 -f flv -b 512k -s 400x300 -vcodec libx264 -acodec libfaac ".$newvideofile,$results);
            
      
        //-f flv -b 768 -ar 22050 -ab 96 -s 720x576

        $vname = $this->input->post('videotitle');
        $vdiscr = $this->input->post('videodescription');
        $vdate = $this->input->post('videodate');
        

        if ($this->uploadmodel->UploadData($catagory,$vname,$vdiscr,$vdate,$if,$vf) == TRUE) // the information has therefore been successfully saved in the db
        {
                redirect('upload/succsess');   // or whatever logic needs to occur
        }
        else
        {
        echo 'An error occurred saving your information. Please try again later';
        
        $this->session->unset_userdata('logged_in');
        session_destroy();
        
        redirect('authentication');
        }
        }
        } 
    }
    else{
        $this->load->view('authentication');
    }


}

function succsess()
{
    $this->load->view('succsess');
}

function addCategory()
{
    $catagory = $this->input->post('catg');
    $location = $this->input->post('loc');
    $locationnew.=$location.'/';
    $this->uploadmodel->addCategory($catagory,$locationnew);
    mkdir($location,0777);
}

}

?>