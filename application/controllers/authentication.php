<?php

class Authentication extends CI_Controller {

	function __construct()
	{
 		parent::__construct();
		$this->load->library('form_validation');
                $this->load->library('session');
		$this->load->database();
		$this->load->helper('form');
                $this->load->helper('array');
		$this->load->helper('url');
		$this->load->model('authenticationmodel');
	}
	function index()
	{
		$this->form_validation->set_rules('username', 'UserName', 'required|max_length[25]');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[25]');

		$this->form_validation->set_error_delimiters('<div class="error" style="position: relative;color:red">', '</div>');

		if ($this->form_validation->run() == FALSE) // validation hasn't been passed
		{
			$this->load->view('authentication');
		}
		else // passed validation proceed to post success logic
		{
		 	// build array for the model

                    $uname = $this->input->post('username');
                    $pwd = $this->input->post('password');

                    $this->session->set_userdata('user', 'Login');

                    $result=$this->authenticationmodel->Authenticate($uname,$pwd);

                    $sess_array = array(
                        'uname' => $uname,
                        'pwd' => $pwd
                    );
                   
			// run insert model to write data to db

			if ($this->authenticationmodel->Authenticate($uname,$pwd) == TRUE) // the information has therefore been successfully saved in the db
			{
                                $this->session->set_userdata('logged_in', $sess_array);
                                $this->session->set_userdata('user', $sess_array["uname"]);
				redirect('upload/index');   // or whatever logic needs to occur
			}
			else
			{
                        
			echo 'An error occurred saving your information. Please try again later';
                        $this->session->set_userdata('error', 'Incorrect Login !');
			redirect('authentication');
			}
		}
             //session_destroy();   
	}
        function logout()
	{
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('error');
            session_destroy();
            redirect('authentication');
        }

}
?>