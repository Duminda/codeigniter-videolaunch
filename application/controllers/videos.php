<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('videoloadmodel');
    }

    public function index()
    {
            $data['files'] = $this->videoloadmodel->get_lbr_video_file_names();
            $data['title'] = 'Videos';
            
            $data['data'] = $this->videoloadmodel->retriveData('lbr');
            $this->load->view('loadLBRVideos',$data);
    }

    public function index2()
    {
            $data['files'] = $this->videoloadmodel->get_lbo_video_file_names();
            $data['title'] = 'Videos';

            $data['data'] = $this->videoloadmodel->retriveData('lbo');
            $this->load->view('loadLBOVideos',$data);
    }
}
?>
