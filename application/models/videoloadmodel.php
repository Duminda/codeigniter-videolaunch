<?php

class Videoloadmodel extends CI_Model {

	public function __construct()
	{
            $this->load->helper('file');
            $this->load->helper('directory');
            $this->load->helper('array');
            $this->load->helper('url');
            $this->load->database();
	}

        public function get_lbr_video_file_names()
        {
                $html="";
                $url=  base_url().'uploads/lbr/';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
		$html.= curl_exec($curl);
		curl_close ($curl);

            $result = get_filenames($url);


            preg_match_all("/<a href=\"(.*flv)\">/i", $html, $matches, PREG_SET_ORDER);
            $i=0;
            foreach ($matches as $val) {
                //echo  $val[1] . "\n";
                $result[$i]=$val[1];
                $i++;
            }
           return $result;

        }

        public function get_lbo_video_file_names()
        {
                $html="";
                $url=base_url().'uploads/lbo/';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
		$html.= curl_exec($curl);
		curl_close ($curl);

            $result = get_filenames($url);


            preg_match_all("/<a href=\"(.*flv)\">/i", $html, $matches, PREG_SET_ORDER);
            $i=0;
            foreach ($matches as $val) {
                //echo  $val[1] . "\n";
                $result[$i]=$val[1];
                $i++;
            }
           return $result;

        }


        public function retriveData($type)
        {
            $this->db->order_by("VideoDate", "asc");
            //$query = $this->db->get('videodata');
            $query = $this->db->get_where('videodata', array('VideoType' => $type));
            $result=$query->result_array();
            return $result;
        }

}
