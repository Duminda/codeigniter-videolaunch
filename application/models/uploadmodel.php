<?php
class Uploadmodel extends CI_Model {

	public function __construct()
	{
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->helper('form');
            $this->load->helper('url');
            $this->load->helper('html');
	}

        function UploadData($catagory,$vname,$vdiscr,$vdate,$vthumb,$video)
	{
                $data = array(
               'VideoType' => $catagory ,
               'VideoTitle' => $vname ,
               'VideoDescription' => $vdiscr ,
               'VideoDate' => $vdate,
               'VideoThumbImage' => $vthumb,
               'VideoPath' => $video,
                );

                $this->db->insert('videodata', $data);
                
		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}

		return FALSE;
        
	}

        function RetriveCatagory()
	{
            $query = $this->db->get('vmscatagory');
            $result1=$query->result_array();
            return $result1;
	}
        
        function RetriveVideoLocation($catagory)
	{
            $query = $this->db->get_where('vmscatagory', array('Catagory' => $catagory));
            $result2=$query->result_array();
            return $result2;
	}

        function addCategory($catName,$catLoc)
        {
            $data = array(
               'Catagory' => $catName ,
               'VideoLocation' => $catLoc
            );

            $this->db->insert('vmscatagory', $data);
        }

}

?>
