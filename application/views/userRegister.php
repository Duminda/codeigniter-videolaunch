<html>
    <head>
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/structure.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/form.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/theme.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/blueimp/bootstrap.min.css"/>
    </head>
    <body>

<?php echo form_open('registration'); ?>
	<div class="info" style="background-image:url('<?php echo base_url();?>images/vms_v_uploader_header.gif');background-repeat: no-repeat; height: 93px"></div>
        <div style="text-align: left;margin: 10px 10px 10px 10px;"><h3 style="color: #ffffff">User Registration</h3>
  <ul>
  <li>
      <label class="desc" style="color: #ffffff">User Name<span class="required"></span>*</label>
        <input id="username" type="text" name="username" maxlength="25" value="<?php echo set_value('username'); ?>" class="field text small" />
        <?php echo form_error('username'); ?>
  </li>
  <li>
        <label class="desc" style="color: #ffffff;">Password<span class="required"></span>*</label>
        <input id="password" type="password" name="password" maxlength="25" value="<?php echo set_value('password'); ?>" class="field text small" />
        <?php echo form_error('password'); ?>
  </li>
  <li>
        <label class="desc" style="color: #ffffff;">Re Enter Password<span class="required"></span>*</label>
        <input id="repassword" type="password" name="repassword" maxlength="25" value="<?php echo set_value('repassword'); ?>" class="field text small" />
        <?php echo form_error('repassword'); ?>
  </li>
  <li>
        <button type="submit" class="btn btn-success fileinput-button" id="reg">
        <!--<i class="icon-upload icon-white"></i>-->
        <span>Register</span>
        </button>
  </li>
  <li>
  <div style="color: red;text-align: left"><?php echo $this->session->userdata('error');?></div>
  </li>
  </ul>
  </div>
<?php echo form_close(); ?>
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery-1.7.2.js"></script>       
<script type="text/javascript" src="<?php echo base_url();?>scripts/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/vms.lbr.js"></script>          
</body>
</html>
