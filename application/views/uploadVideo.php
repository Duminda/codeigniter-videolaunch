<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VMS File Upload </title>

<!-- Bootstrap CSS Toolkit styles -->
<link rel="stylesheet" href="<?php echo base_url();?>scripts/blueimp/bootstrap.min.css"/>
<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
<link rel="stylesheet" href="<?php echo base_url();?>scripts/blueimp/bootstrap-responsive.min.css"/>
<!-- Bootstrap Image Gallery styles -->
<link rel="stylesheet" href="<?php echo base_url();?>scripts/blueimp/bootstrap-image-gallery.min.css"/>
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo base_url();?>scripts/jquery.fileupload-ui.css"/>
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]><script src=<?php //echo base_url();?>scripts/blueimp/html5.js"></script><![endif]-->

<link rel="stylesheet" href="<?php echo base_url();?>scripts/css/jquery-ui-1.8.21.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>scripts/structure.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>scripts/form.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>scripts/theme.css" type="text/css" />
</head>

<body>

<div id="container">

<?php echo form_open_multipart('upload',array('id' => 'fileupload','name' => 'form1'));?>

	<div class="info" style="background-image:url('<?php echo base_url();?>images/vms_v_uploader_header.gif');background-repeat: no-repeat; height: 93px">

	</div>
    <div style="float: right"><?php echo "Welcome ".$this->session->userdata('user');?> | <a href="<?php echo base_url();?>index.php/authentication/logout">Logout</a></div>
	<ul>
        <li>
        <?php $data = $this->uploadmodel->RetriveCatagory();?>
	<label class="desc">Video Category<span class="required"></span></label>
		<div>
                <span class="right">
		<select class="field select addr" name="videocategory">
                <?php $i=count($data);$j=0; while ($j<$i){?>
		<option value="<?php echo $data[$j]["Catagory"];?>"><?php echo $data[$j]["Catagory"];?></option>
                <?php $j++; }?>
		</select>
		</span>
                <br/>
                <br/>
		<span class="right" >
                <button type="button" class="btn btn-primary start" id="addcategory">
		<!--<i class="icon-upload icon-white"></i>-->
		<span>Add New Category</span>
		</button>
                </span>
                </div>
	</li>
	<li>
	<label class="desc">Video Title<span class="required"></span>*</label>
		<div>
		<input class="field text small" type="text" maxlength="25" value="<?php echo set_value('videotitle'); ?>" name="videotitle" id="videotitle"/>
		<?php echo form_error('videotitle'); ?>
		</div>
	</li>

	<li>
	<label class="desc">Video Description<span class="required">*</span></label>
		<div>
		<textarea rows="10" cols="40" class="field textarea small" maxlength="100" name="videodescription" id="videodescription" value="<?php echo set_value('videodescription'); ?>" ></textarea>
		<?php echo form_error('videodescription'); ?>
		</div>

	</li>

	<li>
	<label class="desc">Video Date<span class="required">*</span></label>
		<div>
		<input class="field text small" type="text" maxlength="25" value="<?php echo set_value('videodate'); ?>" name="videodate" id="videodate"/>
		<?php echo form_error('videodate'); ?>
		</div>
	</li>

	<li>
	<label class="desc">Video Name</label>
		<div>
		<input class="field text small" type="text" maxlength="25" value="<?php echo set_value('videoname'); ?>" name="videoname" id="videoname"/>
		<?php echo form_error('videoname'); ?>
		</div>
	</li>

	<li class="complex">
	<label class="desc">Thumb Size</label></li>
	<div>
	<li>	<span class="left">
		<select class="field select addr" name="width">
		<option value="240">240</option>
		<option value="120">120</option>
		<option value="60">60</option>
		</select>X
		</span>
		
		<span class="left">
		<select class="field select addr" name="height">
		<option value="240">240</option>
		<option value="120">120</option>
		<option value="60">60</option>
		</select>
		</span></li>
	</div>
	

        <li>
	<label class="desc">File Upload</label></li>
		<div class="row fileupload-buttonbar">
		<div class="span7">
		<!-- The fileinput-button span is used to style the file input field as button -->
		<span class="btn btn-success fileinput-button">
		<!--                    <i class="icon-plus icon-white"></i>-->
		<span>Add files...</span>
		<input type="file"  name="video" id="vid" multiple/>
		</span>
		<button type="submit" class="btn btn-primary start">
		<!--                    <i class="icon-upload icon-white"></i>-->
		<span>Start upload</span>
		</button>
		<button type="reset" class="btn btn-warning cancel">
		<!--                    <i class="icon-ban-circle icon-white"></i>-->
		<span>Cancel upload</span>
		</button>
		<button type="button" class="btn btn-danger delete">
		<!--                    <i class="icon-trash icon-white"></i>-->
		<span>Delete</span>
		</button>
		<input type="checkbox" class="toggle"/>
		</div>
		<!-- The global progress information -->
		<div class="span5 fileupload-progress fade">
		<!-- The global progress bar -->
		<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
		<div class="bar" style="width:0%;"></div>
		</div>
		<!-- The extended global progress information -->
		<div class="progress-extended">&nbsp;</div>
		</div>
		</div>
                <div class="fileupload-loading"></div>
                <br/>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
                
	</ul>

<?php echo form_close(); ?>
</div>


<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3 class="modal-title"></h3>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>

</div>
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
<!--        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else if (o.files.valid && !i) { %}-->
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
<!--                    <i class="icon-upload icon-white"></i>-->
                    <span>{%=locale.fileupload.start%}</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
<!--                <i class="icon-ban-circle icon-white"></i>-->
                <span>{%=locale.fileupload.cancel%}</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">

{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>

        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"/></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
<!--                <i class="icon-trash icon-white"></i>-->
                <span>{%=locale.fileupload.destroy%}</span>
            </button>
            <input type="checkbox" name="delete" value="1"/>
        </td>
    </tr>
{% } %}
{%


%}
</script>

<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery-1.7.2.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/blueimp/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/blueimp/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/blueimp/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/blueimp/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/blueimp/bootstrap-image-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/jquery.fileupload-ui.js"></script>
<!-- The localization script -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/locale.js"></script>
<!-- The main application script -->
<script type="text/javascript" src="<?php echo base_url();?>scripts/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->

<script type="text/javascript" src="<?php echo base_url();?>scripts/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/wufoo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/vms.lbr.js"></script>

<div id="filetypes" title="Restricted file types !" style="display: none">
	<p>Please choose MPEG files only.</p>
</div>
<div id="uploadsuccess" title="Success !" style="display: none">
	<p>Your file successfully uploaded.</p>
</div>
<div id="uploaderror" title="Error !" style="display: none">
	<p>Upload error occurred !.</p>
</div>

<div id="newcatgform" title="Create new category" style="display: none">
    <form action="" method="POST" onSubmit="return false" id="formcatg" title="Create New Catagory">
      <fieldset>
          <br/>
          <label for="category" style="text-align: left;float: left" >Category</label>
        <input type="text" name="catg" id="catg" style="text-align: left;float: right;height: 10px"  id="catg"/>
          <label for="location" style="text-align: left;float: left" >Video Location</label>
        <input type="text" name="loc" id="catg" style="text-align: left;float: right;height: 10px"  value="uploads/" id="loc"/>
        <input type="submit" value="Submit" name="submit" style="display: none"/>
      </fieldset>
    </form>
</div>
<div id="results">


</div>
</body>
</html>
