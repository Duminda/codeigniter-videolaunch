<html>
    <head>
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/structure.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/form.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/theme.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/blueimp/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>scripts/css/jquery-ui-1.8.21.custom.css"/>
    </head>
    <body>

<?php echo form_open('authentication'); ?>
	<div class="info" style="background-image:url('<?php echo base_url();?>images/vms_v_uploader_header.gif');background-repeat: no-repeat; height: 93px"></div>
        <div style="text-align: left;margin: 10px 10px 10px 10px;"><h3 style="color: #ffffff">Login</h3>
  <ul>
  <li>
      <label class="desc" style="color: #ffffff">User Name<span class="required"></span>*</label>
        <input id="username" type="text" name="username" maxlength="25" value="<?php echo set_value('username'); ?>" class="field text small" />
        <?php echo form_error('username'); ?>
  </li>
  <li>
        <label class="desc" style="color: #ffffff;">Password<span class="required"></span>*</label>
        <input id="password" type="password" name="password" maxlength="25" value="<?php echo set_value('password'); ?>" class="field text small" />
        <?php echo form_error('password'); ?>
  </li>
  <li>
        <button type="submit" class="btn btn-success fileinput-button">
        <!--<i class="icon-upload icon-white"></i>-->
        <span>Login</span>
        </button>
      
      <button type="button" class="btn btn-danger fileinput-button" id="register">
        <!--<i class="icon-upload icon-white"></i>-->
        <span>Register</span>
        </button>
  </li>
  <li>
  <div style="color: red;text-align: left"><?php echo $this->session->userdata('error');?></div>
  </li>
  </ul>
  </div>
<?php echo form_close(); ?>
 <script type="text/javascript" src="<?php echo base_url();?>scripts/jquery-1.7.2.js"></script>       
<script type="text/javascript" src="<?php echo base_url();?>scripts/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>scripts/vms.lbr.js"></script>  
</body>        
   
</html>
